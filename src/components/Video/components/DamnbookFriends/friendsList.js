export default [
    {
        name: "Александр Дуб",
        avatar: '01.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/58e78aa7-da67-4e27-8f60-f309ab7c79fb/1.ism/manifest',
    },
    {
        name: "Макс Иванов",
        avatar: '02.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/29559b0a-512d-41b6-91c6-986954047f0e/2.ism/manifest',
    },
    {
        name: "Ира Смолова",
        avatar: '03.png',
        friendCount: 3,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/e3da57fa-5688-4bc8-ab57-6b4ccc80b695/3.ism/manifest',
    },
    {
        name: "Игорь Самойлов",
        avatar: '04.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/ea48e7c6-c7c7-4ba8-80d4-a169e36e2153/4.ism/manifest',
    },
    {
        name: "Jane Moreynis",
        avatar: '05.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/6dac4851-41fa-4ed9-9938-cbc0582e0e95/5.ism/manifest',
    },
    {
        name: "Яна Костылева",
        avatar: '06.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/19a6d8a8-2edb-4b5f-a444-db2afc665ccf/6.ism/manifest',
    },

    {
        name: "Роман Ненашев",
        avatar: '07.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/84af9b1c-daae-4367-b029-4797c09893ba/7.ism/manifest',
    },
    {
        name: "Аня Китаева",
        avatar: '08.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/71264643-296e-4ca9-82c5-42105381b95c/8.ism/manifest',
    },
    {
        name: "Айаз Кая",
        avatar: '09.png',
        friendCount: 13,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/50a8103b-3864-430a-92b9-67c90c9e025e/9.ism/manifest',
    },
    {
        name: "Челси Мэннинг",
        avatar: '10.png',
        friendCount: 2,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/f4127dde-d307-43ed-a37b-92a2e52bb414/10.ism/manifest',
    },
    {
        name: "Иван Колесов",
        avatar: '11.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/b0bbcbf9-ddda-4e74-88c6-410c57d84509/11.ism/manifest',
    },
    {
        name: "Klaus Jones",
        avatar: '12.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/456cf73c-dfaa-457e-a203-ed2b7f68b1e6/12.ism/manifest',
    },

    {
        name: "Джулиан Уайт",
        avatar: '13.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/f235eae6-8db8-493f-be85-2002438f4e2f/13.ism/manifest',
    },
    {
        name: "Рита Симоненко",
        avatar: '14.png',
        friendCount: 0,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7306f643-8abb-4e1e-b902-cb14a6665c4e/14.ism/manifest',
    },
    {
        name: "Егор Горин",
        avatar: '15.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/f5c65dec-b502-4907-a1ac-4f0aad19a5fd/15.ism/manifest',
    },
    {
        name: "Артур Николаев",
        avatar: '16.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/63cbedaf-e719-428e-85f7-7815322fb671/16.ism/manifest',
    },
    {
        name: "Армен Абрамян",
        avatar: '17.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/59fa5be9-bb28-48fe-86da-176e234a11a0/17.ism/manifest',
    },
    {
        name: "Dude Dudenstein",
        avatar: '18.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/16e1eab3-a577-4e9d-ad4c-9b8422c7376f/18.ism/manifest',
    },

    {
        name: "Виктория Ефремова",
        avatar: '19.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/837c6b1a-936a-46df-8ebb-14614bd27c40/19.ism/manifest',
    },
    {
        name: "Tema Razumovsky",
        avatar: '20.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/2ed0f2b1-fa45-4a2f-9e07-215cf9e68f83/20.ism/manifest',
    },
    {
        name: "Георгий Шотов",
        avatar: '21.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/95eb28ba-b492-4372-be7c-658a1b0e1b89/21.ism/manifest',
    },
    {
        name: "Виталий Герасимов",
        avatar: '22.png',
        friendCount: 0,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/5a681d53-e4bd-40b9-8a00-81a721fe8f1d/22.ism/manifest',
    },
    {
        name: "Полина Ершова",
        avatar: '23.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/e017fc2a-ee7a-416d-92ec-23fd28bbc633/23.ism/manifest',
    },
    {
        name: "Светлана Хлюпина",
        avatar: '24.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/510afb33-d6e7-426c-be6b-6397024a6b7c/24.ism/manifest',
    },
]
